import requests
import csv
import json
import base64
import argparse


def main(args):
    if not args.url.endswith("/"):
        url = f"{args.url}/wp-json/wp/v2"
    else:
        url = f"{args.url}wp-json/wp/v2"

    print(url)    

    # Get token
    wp_connection = f"{args.user}:{args.password}"
    token = base64.b64encode(wp_connection.encode())
    # Requests headers
    headers = {'Authorization': 'Basic ' + token.decode("utf-8")}

    # Load articles from CSV & insert into Wordpress
    articles = []
    with open(args.file, newline='') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=args.sep)
        for row in reader:
            # Prepare post data
            post = {
                "title": row["post_title"],
                "status": "publish",
                "content": row["post_content"],
                "author": 1,
                "format": "standard",
            }
            r = requests.post(url + "/posts", headers=headers, json=post, verify=False)
            print(row["post_title"], r.status_code)
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Post articles to Wordpress')
    parser.add_argument('--file', help='CSV file with articles', required=True, type=str)
    parser.add_argument('--sep', help='CSV separator', required=False, default=";", type=str)
    parser.add_argument('--url', help='Wordpress URL', required=True, type=str)
    parser.add_argument('--user', help='Wordpress user', required=True, type=str)
    parser.add_argument('--password', help='Wordpress password', required=True, type=str)
    parser.add_argument('--no_ssl_verify', help='Disable SSL verification', required=False, default=True, action='store_false')
    args = parser.parse_args()

    main(args)

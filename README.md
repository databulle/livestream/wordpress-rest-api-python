# Python WP bulk posts

Example script to bulk import posts into WordPress from a CSV file, using [WordPress REST API](https://developer.wordpress.org/rest-api/).  

Code from live stream on [Twitch](https://www.twitch.tv/diije), replay available on [YouTube](https://www.youtube.com/@databulle).  

## Setup

### WordPress

First of all you'll need a WordPress blog with SSL (https) enabled to use the REST API.  
I used Docker to run a local instance. If you want to do this, see:  
- [How to run WordPress locally on macos with Docker Compose](https://catalins.tech/how-to-run-wordpress-locally-on-macos-with-docker-compose/),  
- [How to use HTTPS in Docker for local development](https://dev.to/vishalraj82/using-https-in-docker-for-local-development-nc7),  
- The `docker-compose.yml` file and the `proxy` directory in this repository.  

You'll also need to create an application password. Go to the admin interface, then _Users_ > _Profile_ and you'll find the form at the bottom of the page. Remember to save the generated password, as you'll need it later ;)  

### Python

Create a virtual environment using your prefered utility. I use [pyenv](https://github.com/pyenv/pyenv):  

    pyenv virtualenv 3.11.2 wordpress
    pyenv local wordpress

Install dependencies:  

    pip install -r requirements.txt

## Usage

Available options:   
- `--file`: CSV file with articles  
- `--sep`: CSV separator  
- `--url`: Wordpress URL  
- `--user`: Wordpress user  
- `--password`: Wordpress application password  
- `--no_ssl_verify`: Disable SSL verification (useful for self-generated SSL certificates)  

Example:  

    python post_articles.py --url https://www.example.com/ --user johndoe --password 123456 --file articles.csv  

